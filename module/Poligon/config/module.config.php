<?php

namespace Poligon;

use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'router' => [
        'routes' => [
            'tank' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/tank[/:action]',
                    'defaults' => [
                        'controller' => Controller\TankController::class,
                        'action' => 'index',
                    ],
                ],
            ],
            'topol' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/topol[/:action]',
                    'defaults' => [
                        'controller' => Controller\TopolController::class,
                        'action' => 'index',
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\TankController::class => Controller\Factory\DoctrineControllerFactory::class,
            Controller\TopolController::class => Controller\Factory\DoctrineCOntrollerFactory::class,
        ],
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',

        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
    'doctrine' => [
        'driver' => [
            'orm_entity' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'paths' => [__DIR__ . '/../src/Entity']
            ],
            'orm_default' => [
                'drivers' => [
                    'Poligon\Entity' => 'orm_entity',
                ]
            ],

        ],
    ],
    /*'service_manager' => [
        'factories' => [
            Service\AuthDoctrineAdapter::class => Service\Factory\DoctrineServiceFactory::class
        ]
    ]*/
];