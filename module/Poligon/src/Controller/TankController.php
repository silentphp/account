<?php
/**
 * Created by PhpStorm.
 * User: serv
 * Date: 25.02.18
 * Time: 10:38
 */
namespace Poligon\Controller;

use Doctrine\ORM\EntityManager;
use http\Env\Request;
use Poligon\Entity\Tank;
use Poligon\Form\TankForm;
use Zend\Debug\Debug;
use Zend\Form\Element;
use Zend\Form\Form;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class TankController extends AbstractActionController
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function indexAction()
    {
        //$tank = $this->em->getRepository(Tank::class)->findAll();

        //Debug::dump($tank);

        $tank = new Tank();
        $form = new TankForm($this->em);
        $form->bind($tank);
        /*$request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $this->entityManager->persist($tank);
                $this->entityManager->flush();
                return $this->redirect()->toRoute('tank');
            }
        }*/

        /*$name = new Element('name');
        $name->setLabel('Your name');
        $name->setAttributes([
            'type' => 'text',
        ]);
        $form = new Form('contact');
        $form->add($name);*/


        return new ViewModel(['form' => $form]);
    }


}