<?php

namespace Poligon\DTO;
use Poligon\Entity;

class ElementTankDto
{
    /*private $id;

    private $name;

    private $category;*/

    private $element;

   // private $model;

    public function __construct(Entity\ElementTank $elementTank)
    {
        $this->element = $elementTank;
        //$this->model = 'spec';
       // $this->name = $elementTank->getNameTank();
       // $this->category = $elementTank->getCategory();
    }

    /**
     * @return Entity\ElementTank
     */
    public function getElement(): Entity\ElementTank
    {
        return $this->element;
    }

    /**
     * @return string
     */
    public function getTime(): string
    {
        $time = time();
        return $time;
    }

}