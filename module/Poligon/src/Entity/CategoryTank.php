<?php


namespace Poligon\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class CategoryTank
 * @ORM\Entity
 */
class CategoryTank
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var
     *
     *  @ORM\OneToMany(targetEntity="ElementTank", mappedBy="category")
     */
    private $elementTank;

    public function __construct() {
        $this->elementTank = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return CategoryTank
     */
    public function setId(int $id): CategoryTank
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return CategoryTank
     */
    public function setName(string $name): CategoryTank
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getElementTank()
    {
        return $this->elementTank;
    }

    /**
     * @param mixed $elementTank
     * @return CategoryTank
     */
    public function setElementTank($elementTank)
    {
        var_dump($elementTank);
        $this->elementTank = $elementTank;
        return $this;
    }


}