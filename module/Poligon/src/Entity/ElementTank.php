<?php


namespace Poligon\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class ElementTank
 * @ORM\Entity
 */
class ElementTank
{

    /**
     * @var int
     * @ORM\Id
     *
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $nameTank;

    /**
     * @var CategoryTank
     *
     * @ORM\ManyToOne(targetEntity="CategoryTank", inversedBy="elementTank")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return ElementTank
     */
    public function setId(int $id): ElementTank
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getNameTank(): string
    {
        return $this->nameTank;
    }

    /**
     * @param string $nameTank
     */
    public function setNameTank(string $nameTank)
    {
        $this->nameTank = $nameTank;
    }

    /**
     * @return CategoryTank
     */
    public function getCategory(): CategoryTank
    {
        return $this->category;
    }

}