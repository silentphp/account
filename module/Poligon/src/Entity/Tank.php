<?php
/**
 * Created by PhpStorm.
 * User: serv
 * Date: 25.02.18
 * Time: 10:45
 */

namespace Poligon\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Tank
 *
 * @ORM\Entity
 * @ORM\Table(name="tank")
 */
class Tank
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

}