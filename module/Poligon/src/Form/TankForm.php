<?php
/**
 * Created by PhpStorm.
 * User: serv
 * Date: 25.02.18
 * Time: 11:03
 */

namespace Poligon\Form;


use DoctrineModule\Stdlib\Hydrator\DoctrineObject;
use Poligon\Entity\Tank;
use Zend\Form\Form;

class TankForm extends Form
{
    public function __construct($entityManager)
    {
        parent::__construct('tank-form');
        $this->setAttribute('method', 'post');
        $this->setHydrator(new DoctrineObject($entityManager, Tank::class));
        $this->add([
            'type' => 'hidden',
            'name' => 'id',
        ]);
        $this->add([
            'type' => 'text',
            'name' => 'name',
            'attributes' => [
                'id' => 'title'
            ],
            'options' => [
                'label' => 'Title',
            ],
        ]);
        $this->add([
            'type' => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Create',
                'id' => 'submitbutton',
            ],
        ]);
    }

}