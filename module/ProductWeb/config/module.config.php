<?php

namespace ProductWeb;

use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'router' => [
        'routes' => [
            'list' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/web/list[/:page]',
                    'defaults' => [
                        'controller' => Controller\ProductWebController::class,
                        'action' => 'index',
                    ],
                ],
            ],
            'basket' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/web/basket[/:action[/:id][/:quantity]]',
                    'defaults' => [
                        'controller' => Controller\BasketController::class,
                        'action' => 'index',
                    ],
                ],
            ],
            'list-basket' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/web/list-basket',
                    'defaults' => [
                        'controller' => Controller\BasketController::class,
                        'action' => 'listBasket',
                    ],
                ],
            ],
            'set-delivery' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/web/set-delivery[/:id]',
                    'defaults' => [
                        'controller' => Controller\BasketController::class,
                        'action' => 'set-delivery',
                    ],
                ],
            ],
            'fill' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/web/fill',
                    'defaults' => [
                        'controller' => Controller\OrderController::class,
                        'action' => 'fill',
                    ],
                ],
            ],
            'create-order' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/web/create-order',
                    'defaults' => [
                        'controller' => Controller\OrderController::class,
                        'action' => 'create-order',
                    ],
                ],
            ],
            'thank-order' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/web/thank-order[/:id]',
                    'defaults' => [
                        'controller' => Controller\OrderController::class,
                        'action' => 'thank-order',
                    ],
                ],
            ],
            'order-list' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/web/order-list',
                    'defaults' => [
                        'controller' => Controller\OrderController::class,
                        'action' => 'order-list',
                    ],
                ],
            ],
            'city' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/web/city[/:action[/:id]]',
                    'defaults' => [
                        'controller' => Controller\CityController::class,
                        'action' => 'city'
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\ProductWebController::class => Controller\Factory\DoctrineControllerFactory::class,
            Controller\BasketController::class => Controller\Factory\DoctrineControllerFactory::class,
            Controller\OrderController::class => Controller\Factory\OrderControllerFactory::class,
            Controller\CityController::class => Controller\Factory\DoctrineControllerFactory::class
        ],
    ],
    'service_manager' => [
        'factories' => [
            Service\OrderService::class => Service\Factory\OrderServiceFactory::class
        ]
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',

        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
    'doctrine' => [
        'driver' => [
            'orm_entity' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'paths' => [__DIR__ . '/../src/Entity']
            ],
            'orm_default' => [
                'drivers' => [
                    'ProductWeb\Entity' => 'orm_entity',
                ]
            ],

        ],
    ],
];