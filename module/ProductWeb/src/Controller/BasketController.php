<?php


namespace ProductWeb\Controller;


use Doctrine\ORM\EntityManager;
use ProductWeb\Entity\Basket;
use ProductWeb\Entity\City;
use ProductWeb\Entity\Delivery;
use ProductWeb\Entity\ProductWeb;
use ProductWeb\Entity\UserBasket;
use Zend\Http\Header\SetCookie;
use Zend\Http\PhpEnvironment\Request;
use Zend\Http\PhpEnvironment\Response;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\Exception\BadMethodCallException;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;

class BasketController extends AbstractActionController
{
    /**
     * @var EntityManager
     */
    private $em;


    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @return ViewModel
     */
    public function listBasketAction()
    {
        /** @var Request $request */
        $request = $this->getRequest();
        //$uniqid = !empty($request->getCookie()->uniqid) ? $request->getCookie()->uniqid : '';

        if (empty($request->getCookie()->uniqid)) {
            return new ViewModel();
        }

        /** @var UserBasket $userBasket */
        $userBasket = $this->em->getRepository(UserBasket::class)
            ->findOneBy(['uniqid' => $request->getCookie()->uniqid]);
        if ($userBasket->isHasOrder()) {
            return new ViewModel();
        }

        $basket = $this->em->getRepository(Basket::class)->findBy(['userBasket' => $userBasket]);

        $cityId = 1;
        if (!empty($request->getCookie()->city)) {
            $cityId = $request->getCookie()->city;
        }
        /** @var City $city */
        $city = $this->em->getRepository(City::class)->find($cityId);
        /** @var Delivery[] $delivery */
        $delivery = $this->em->getRepository(Delivery::class)->findBy(['city' => $city]);

        if (empty($delivery)) {
            return new ViewModel(['basket' => $basket]);
        }

        $deliverySession = new Container('delivery');
        $idDelivery = $deliverySession->offsetGet('id');
        if (empty($idDelivery)) {
            $idDelivery = $delivery[0]->getId();
            $deliverySession->offsetSet('id', $idDelivery);
        }
        /** @var Delivery $selectDelivery */
        $selectDelivery = $this->em->getRepository(Delivery::class)->find($idDelivery);

        if ($selectDelivery->getCity()->getId() != $city->getId()) {
            $idDelivery = $delivery[0]->getId();
            $selectDelivery = $this->em->getRepository(Delivery::class)->find($idDelivery);
        }

        return new ViewModel(['basket' => $basket, 'delivery' => $delivery, 'select_delivery' => $selectDelivery]);
    }

    public function setDeliveryAction()
    {
        $id = $this->params('id');
        $deliverySession = new Container('delivery');
        $deliverySession->offsetSet('id', $id);
        return $this->redirect()->toRoute('list-basket', ['action' => 'list-basket']);
    }

    /**
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addToBasketAction()
    {
        $productId = (int)$this->params('id');
        $quantity = (int)$this->params('quantity');
        if ($productId <= 0) {
            throw new BadMethodCallException("ProductId is required");
        }
        if ($quantity <= 0) {
            throw new BadMethodCallException("Quantity is required");
        }

        $userBasket = $this->restoreUserBasket();
        if ($userBasket->isHasOrder()) {
            return $this->redirect()->toRoute('list');
        }

        /** @var ProductWeb $product */
        $product = $this->em->getRepository(ProductWeb::class)->find($productId);

        /** @var Basket $result */
        $basket = $this->em->getRepository(Basket::class)->findOneBy(['userBasket' => $userBasket, 'product' => $product]);
        if (!$basket) {
            $basket = new Basket();
        }

        $basket->setProduct($product)->addQuantity($quantity)->setUserBasket($userBasket);

        $this->em->persist($basket);
        $this->em->flush();

        return $this->redirect()->toRoute('list');
    }

    private function restoreUserBasket(): UserBasket
    {
        /** @var Request $request */
        $request = $this->getRequest();
        $uniqueId = $request->getCookie()->uniqid ?? null;
        if ($uniqueId) {
            /** @var UserBasket $userBasket */
            $userBasket = $this->em->getRepository(UserBasket::class)->findOneBy(['uniqid' => $uniqueId]);
        } else {
            $userBasket = $this->createUserBasket();
            $this->rememberUniqueId($userBasket->getUniqid());
        }

        return $userBasket;
    }

    /**
     * @param $uniqueId
     */
    private function rememberUniqueId($uniqueId)
    {
        $cookie = new SetCookie('uniqid', $uniqueId, strtotime('+30 days'), '/');
        /** @var Response $response */
        $response = $this->getResponse();
        $response->getHeaders()->addHeader($cookie);
    }

    /**
     * @return UserBasket
     */
    private function createUserBasket(): UserBasket
    {
        $userBasket = new UserBasket();
        $userBasket->setUniqid(uniqid());
        $userBasket->setHasOrder(false);
        $this->em->persist($userBasket);

        return $userBasket;
    }

}