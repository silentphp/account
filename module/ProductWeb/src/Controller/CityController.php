<?php


namespace ProductWeb\Controller;


use Doctrine\ORM\EntityManager;
use ProductWeb\Entity\City;
use Zend\Http\Header\SetCookie;
use Zend\Http\PhpEnvironment\Response;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class CityController extends AbstractActionController
{
    /** @var  EntityManager */
    private $em;

    function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function cityAction()
    {
        $city = $this->em->getRepository(City::class)->findAll();

        return new ViewModel(['city' => $city]);
    }

    public function setCityAction()
    {
        $cookie = new SetCookie('city', $this->params('id'), strtotime('+30 days'), '/');
        /** @var Response $response */
        $response = $this->getResponse();
        $response->getHeaders()->addHeader($cookie);
        return $this->redirect()->toRoute('list');
    }

}