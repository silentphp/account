<?php


namespace ProductWeb\Controller\Factory;


use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use ProductWeb\Controller\OrderController;
use ProductWeb\Service\OrderService;
use Zend\ServiceManager\Factory\FactoryInterface;

class OrderControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get(EntityManager::class);
        $orderService = $container->get(OrderService::class);

        return new OrderController($entityManager, $orderService);
    }

}