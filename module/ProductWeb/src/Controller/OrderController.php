<?php


namespace ProductWeb\Controller;


use Doctrine\ORM\EntityManager;
use ProductWeb\Entity\Basket;
use ProductWeb\Entity\Delivery;
use ProductWeb\Entity\Order;
use ProductWeb\Entity\UserBasket;
use ProductWeb\Form\OrderForm;
use ProductWeb\Service\OrderService;
use Zend\Http\Header\SetCookie;
use Zend\Http\PhpEnvironment\Request;
use Zend\Http\PhpEnvironment\Response;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;

class OrderController extends AbstractActionController
{
    /** @var EntityManager */
    private $em;

    /** @var OrderService */
    private $orderService;

    /**
     * OrderController constructor.
     * @param EntityManager $em
     * @param OrderService $orderService
     */
    function __construct(EntityManager $em, OrderService $orderService)
    {
        $this->em = $em;
        $this->orderService = $orderService;
    }

    /**
     * @return \Zend\Http\Response|ViewModel
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function fillAction()
    {
        $form = new OrderForm($this->em);
        return new ViewModel(['form' => $form]);
    }

    public function createOrderAction()
    {
        $orderDataForm = new Order();
        $form = new OrderForm($this->em);
        $form->bind($orderDataForm);
        /** @var Request $request */
        $request = $this->getRequest();
        $form->setData($request->getPost());

        if (!$request->isPost()) {
            return new ViewModel();
        }
        if (!$form->isValid()) {
            return new ViewModel();
        }
        $uniqid = $request->getCookie()->uniqid;
        $userBasket = $this->em->getRepository(UserBasket::class)->findOneBy(['uniqid' => $uniqid]);
        $basket = $this->em->getRepository(Basket::class)->findBy(['userBasket' => $userBasket]);

        $deliverySession = new Container('delivery');
        //var_dump($deliverySession);
        /** @var Delivery $delivery */
        $delivery = $this->em->getRepository(Delivery::class)->find($deliverySession->offsetGet('id'));

        $order = $this->orderService->createOrder($orderDataForm, $delivery, $basket);

        $deliverySession->offsetUnset('id');
        $this->removeCookieUniqid();

        /** @var UserBasket $userBasket */
        $userBasket->setHasOrder(true);
        $this->em->persist($userBasket);
        $this->em->flush();

        return $this->redirect()->toRoute('thank-order', ['action' => 'thank-order', 'id' => $order->getId()]);
    }

    public function orderListAction()
    {
        $order = $this->em->getRepository(Order::class)->findAll();
        return new ViewModel(['order' => $order]);
    }

    public function thankOrderAction()
    {
        $id = $this->params('id');
        return new ViewModel(['id' => $id]);
    }

    private function removeCookieUniqid(): void
    {
        $cookie = new SetCookie('uniqid', '', time(), '/');
        $cookie->setExpires(strtotime('-30 days'));
        /** @var Response $response */
        $response = $this->getResponse();
        $response->getHeaders()->addHeader($cookie);
    }

}