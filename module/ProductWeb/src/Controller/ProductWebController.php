<?php

namespace ProductWeb\Controller;


use Doctrine\ORM\EntityManager;
use DoctrineORMModule\Paginator\Adapter\DoctrinePaginator;
use ProductWeb\Entity\Basket;
use ProductWeb\Entity\City;
use ProductWeb\Entity\ProductWeb;
use ProductWeb\Entity\UserBasket;
use Zend\Http\PhpEnvironment\Request;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Paginator\Paginator;
use Zend\View\Model\ViewModel;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMPaginator;

class ProductWebController extends AbstractActionController
{
    private $em;

    private $userBasketRepository;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;

        $this->userBasketRepository = $this->em->getRepository(UserBasket::class);
    }

    /**
     * @return ViewModel
     */
    public function indexAction()
    {
        $page = $this->params('page');
        $queryBuilder = $this->em->createQueryBuilder();
        $queryBuilder->select('product')->from(ProductWeb::class, 'product');
        $ormPaginator = new ORMPaginator($queryBuilder, false);
        $doctrinePaginator = new DoctrinePaginator($ormPaginator);
        $zendPaginator = new Paginator($doctrinePaginator);
        $zendPaginator->setItemCountPerPage(10);
        $zendPaginator->setCurrentPageNumber($page);

        $countBasket = 0;
        /** @var Request $request */
        $request = $this->getRequest();

        if (!empty($request->getCookie()->uniqid)) {
            /** @var UserBasket $userBasket */
            $userBasket = $this->em->getRepository(UserBasket::class)->findOneBy(['uniqid' => $request->getCookie()->uniqid]);
            if (!$userBasket->isHasOrder()) {
                $basket = $this->em->getRepository(Basket::class)->findBy(['userBasket' => $userBasket]);
                $countBasket = count($basket);
            }
            //$query = $this->em->createQuery('SELECT b FROM ProductWeb\Entity\Basket as b where b.userBasket = :userBasket and b.order is null');
            //$query->setParameter('userBasket', $userBasket[0]->getId());
            //$basketResult = $query->getArrayResult();
        }

        $cityId = 1;
        if (!empty($request->getCookie()->city)) {
            $cityId = $request->getCookie()->city;
        }
        $city = $this->em->getRepository(City::class)->find($cityId);

        return new ViewModel(['product' => $zendPaginator, 'basket' => $countBasket, 'city' => $city]);
    }

}