<?php


namespace ProductWeb\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Basket
 *
 * @ORM\Entity
 * @ORM\Table(name="basket")
 */
class Basket
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var ProductWeb
     *
     * @ORM\ManyToOne(targetEntity="ProductWeb", inversedBy="basket")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     */
    protected $product;

    /**
     * @var UserBasket
     *
     * @ORM\ManyToOne(targetEntity="UserBasket", inversedBy="basket")
     * @ORM\JoinColumn(name="user_basket_id", referencedColumnName="id")
     */
    protected $userBasket;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $price;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $quantity;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var Order
     *
     * @ORM\ManyToOne(targetEntity="Order", inversedBy="basket")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     */
    protected $order;

    /**
     * Basket constructor.
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime("now", new \DateTimeZone( 'Europe/Moscow'));
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Basket
     */
    public function setId(int $id): Basket
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return ProductWeb
     */
    public function getProduct(): ProductWeb
    {
        return $this->product;
    }

    /**
     * @param ProductWeb $product
     * @return self
     */
    public function setProduct(ProductWeb $product): self
    {
        $this->product = $product;
        $this->setPrice($product->getPrice());

        return $this;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @param int $price
     * @return self
     */
    private function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     * @return self
     */
    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @param int $quantity
     * @return self
     */
    public function addQuantity(int $quantity): self
    {
        if( $this->quantity !== null){
            $this->quantity += $quantity;
        } else {
            $this->quantity = $quantity;
        }

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * @return Basket
     */
    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return UserBasket
     */
    public function getUserBasket(): UserBasket
    {
        return $this->userBasket;
    }

    /**
     * @param UserBasket $userBasket
     */
    public function setUserBasket(UserBasket $userBasket)
    {
        $this->userBasket = $userBasket;
    }

    /**
     * @return Order
     */
    public function getOrder(): Order
    {
        return $this->order;
    }

    /**
     * @param Order $order
     * @return Basket
     */
    public function setOrder(Order $order): Basket
    {
        $this->order = $order;
        return $this;
    }

}