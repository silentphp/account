<?php


namespace ProductWeb\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Delivery
 *
 * @ORM\Entity
 *
 */
class Delivery
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $price;

    /**
     * @var City
     *
     * @ORM\ManyToOne(targetEntity="City", inversedBy="delivery")
     * @ORM\JoinColumn(name="city_id", referencedColumnName="id")
     */
    private $city;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Delivery
     */
    public function setId(int $id): Delivery
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Delivery
     */
    public function setName(string $name): Delivery
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @param int $price
     * @return Delivery
     */
    public function setPrice(int $price): Delivery
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return City
     */
    public function getCity(): City
    {
        return $this->city;
    }

    /**
     * @param City $city
     * @return Delivery
     */
    public function setCity(City $city): Delivery
    {
        $this->city = $city;
        return $this;
    }

}