<?php
/**
 * Created by PhpStorm.
 * User: serv
 * Date: 21.02.18
 * Time: 18:20
 */

namespace ProductWeb\Entity\Repository;


use Doctrine\ORM\EntityRepository;
use ProductWeb\Entity\ProductWeb;

class ProductWebRepository extends EntityRepository
{
    public function findProduct()
    {
        $em = $this->getEntityManager();
        $queryBuilder = $em->createQueryBuilder();
        $queryBuilder->select('product')->from(ProductWeb::class, 'product');
        return $queryBuilder;
    }
}