<?php


namespace ProductWeb\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class UserBasket
 *
 * @ORM\Entity
 * @ORM\Table(name="user_basket")
 * @ORM\Entity(repositoryClass="ProductWeb\Entity\Repository\ProductWebRepository")
 */
class UserBasket
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $uniqid;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $hasOrder;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return UserBasket
     */
    public function setId(int $id): UserBasket
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getUniqid(): string
    {
        return $this->uniqid;
    }

    /**
     * @param string $uniqid
     * @return UserBasket
     */
    public function setUniqid(string $uniqid): UserBasket
    {
        $this->uniqid = $uniqid;
        return $this;
    }

    /**
     * @return bool
     */
    public function isHasOrder(): bool
    {
        return $this->hasOrder;
    }

    /**
     * @param bool $hasOrder
     * @return UserBasket
     */
    public function setHasOrder(bool $hasOrder): UserBasket
    {
        $this->hasOrder = $hasOrder;
        return $this;
    }
}