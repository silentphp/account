<?php

namespace ProductWeb\Filter;


use Zend\InputFilter\InputFilter;

class OrderFilter extends InputFilter
{
    function __construct()
    {
        $this->add([
            'name' => 'name',
            'required' => true,
            'validators' => [
                [
                    'name' => 'stringlength',
                    'options' => [
                        'min' => 3,
                        'messages' => array(
                            \Zend\Validator\StringLength::TOO_SHORT => 'Длина должна быть больше %min% символов',

                        ),
                    ],
                ],
                [
                    'name' => 'notempty',
                    'options' => [
                        'messages' => array(
                            \Zend\Validator\NotEmpty::IS_EMPTY => 'Поле не заполнено',

                        ),
                    ],
                ],
            ]
        ]);

    }
}