<?php
/**
 * Created by PhpStorm.
 * User: serv
 * Date: 22.04.18
 * Time: 22:58
 */

namespace ProductWeb\Form;


use Doctrine\ORM\EntityManager;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject;
use ProductWeb\Entity\Order;
use ProductWeb\Filter\OrderFilter;
use Zend\Form\Form;

class OrderForm extends Form
{
    public function __construct($entityManager)
    {
        parent::__construct('order-form');
        $this->setAttribute('method', 'post');
        /** @var EntityManager $entityManager */
        $this->setHydrator(new DoctrineObject($entityManager, Order::class));
        $this->setInputFilter(new OrderFilter());
        $this->add([
            'type' => 'hidden',
            'name' => 'id',
        ]);
        $this->add([
            'type' => 'text',
            'name' => 'name',
            'attributes' => [
                'id' => 'name'
            ],
            'options' => [
                'label' => 'Имя',
            ],
        ]);
        $this->add([
            'type' => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Оформить',
                'id' => 'submitbutton',
            ],
        ]);
    }
}