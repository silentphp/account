<?php
/**
 * Created by PhpStorm.
 * User: serv
 * Date: 21.02.18
 * Time: 11:29
 */

namespace ProductWeb;


class Module
{
    const VERSION = '3.0.3-dev';

    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }
}