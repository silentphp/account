<?php


namespace ProductWeb\Service\Factory;


use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use ProductWeb\Service\OrderService;
use Zend\ServiceManager\Factory\FactoryInterface;

class OrderServiceFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $entityManager = $container->get(EntityManager::class);
        return new OrderService($entityManager);
    }
}