<?php

namespace ProductWeb\Service;


use Doctrine\ORM\EntityManager;
use ProductWeb\Entity\Basket;
use ProductWeb\Entity\Delivery;
use ProductWeb\Entity\Order;

class OrderService
{
    private $em;

    function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param Order $order
     * @param Delivery $delivery
     * @param Basket[] $basket
     * @return mixed
     */
    public function createOrder($order, $delivery, $basket)
    {
        $order->setDelivery($delivery);
        $order->setCreatedAt(new \DateTime("now", new \DateTimeZone('Europe/Moscow')));
        $this->em->persist($order);
        $this->em->flush();
        foreach ($basket as $item) {
            $item->setOrder($order);
        }
        #$this->em->persist($order);
        $this->em->flush();

        return $order;
    }
}