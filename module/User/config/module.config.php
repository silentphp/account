<?php

namespace User;

use User\Controller\Factory\UserControllerFactory;
use User\Service\AuthManager;
use User\Service\Factory\AuthenticationServiceFactory;
use User\Service\Factory\AuthManagerFactory;
use User\Service\Factory\RbacFactory;
use User\Service\Factory\RbacManagerFactory;
use User\Service\RbacManager;
use Zend\Authentication\AuthenticationService;
use Zend\Permissions\Rbac\Rbac;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'router' => [
        'routes' => [
            'user' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/user[/:action[/:hash]]',
                    'defaults' => [
                        'controller' => Controller\UserController::class,
                        'action' => 'index',
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\UserController::class => UserControllerFactory::class,
        ],
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',

        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
    'doctrine' => [
        'driver' => [
            'orm_entity' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'paths' => [__DIR__ . '/../src/Entity']
            ],
            'orm_default' => [
                'drivers' => [
                    'User\Entity' => 'orm_entity',
                ]
            ],

        ],
    ],
    'service_manager' => [
        'factories' => [
            AuthenticationService::class => AuthenticationServiceFactory::class,
            AuthManager::class => AuthManagerFactory::class,
            RbacManager::class => RbacManagerFactory::class,
            Rbac::class => RbacFactory::class
        ]
    ],
    'access_filter' => [
        'controllers' => [
            Controller\UserController::class => [
                [
                    'actions' => ['main', 'change-password'],
                    'allow' => 'view'
                ],
                [
                    'actions' => ['edit-profile'],
                    'allow' => 'edit'
                ]
            ]
        ]
    ]
];