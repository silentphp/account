<?php
/**
 * Created by PhpStorm.
 * User: serv
 * Date: 23.02.18
 * Time: 22:38
 */

namespace User\Controller;


use Doctrine\ORM\EntityManager;
use User\Entity\ResetPassword;
use User\Entity\User;
use User\Form\ChangePasswordForm;
use User\Form\EditProfileForm;
use User\Form\RecoveryPasswordForm;
use User\Form\RegistrationForm;
use User\Form\AuthForm;
use User\Form\ResetPasswordForm;
use User\Service\AuthDoctrineAdapter;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Result;
use Zend\Crypt\Password\Bcrypt;
use Zend\Http\PhpEnvironment\Request;
use Zend\Mail\Message;
use Zend\Mail\Transport\Sendmail;
use Zend\Mime\Part;
use Zend\Mime\Message as MimeMessage;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Permissions\Rbac\Rbac;
use Zend\Permissions\Rbac\Role;
use Zend\View\Model\ViewModel;

class UserController extends AbstractActionController
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var AuthenticationService
     */
    private $authService;

    public function __construct(EntityManager $entityManager, AuthenticationService $authService)
    {
        $this->em = $entityManager;
        $this->authService = $authService;
    }

    public function indexAction()
    {
        return new ViewModel();
    }

    public function authAction()
    {
        if ($this->authService->getIdentity()) {
            return $this->redirect()->toRoute('user', ['action' => 'main']);
        }
        $user = new User();
        $form = new AuthForm($this->em);
        $form->bind($user);
        /** @var Request $request */
        $request = $this->getRequest();
        $messages = [];
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $adapter = new AuthDoctrineAdapter($this->em, $request->getPost()->login, $request->getPost()->password);
                $result = $this->authService->authenticate($adapter);
                if ($result->getCode() == Result::SUCCESS) {
                    return $this->redirect()->toRoute('user', ['action' => 'main']);
                }
                $messages = $result->getMessages();
            }
        }

        return new ViewModel(['form' => $form, 'messages' => $messages]);
    }

    /**
     * @return \Zend\Http\Response|ViewModel
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function registrationAction()
    {
        if ($this->authService->getIdentity()) {
            return $this->redirect()->toRoute('user', ['action' => 'main']);
        }
        $user = new User();
        $form = new RegistrationForm($this->em);
        $form->bind($user);

        $bcrypt = new Bcrypt();

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {

            $form->setData($request->getPost());

            if ($form->isValid()) {
                $user->setPassword($bcrypt->create($request->getPost()->password));
                $user->setName('');
                $user->setActive(1);
                $user->setRole('registered');
                $this->em->persist($user);
                $this->em->flush();
                return $this->redirect()->toRoute('user', array('action' => 'auth'));
            }
        }
        return new ViewModel(['form' => $form]);
    }

    public function logoutAction()
    {
        if ($this->authService->getIdentity()) {
            $this->authService->clearIdentity();
        }
        return $this->redirect()->toRoute('user');
    }

    public function mainAction()
    {
        $parameter['login'] = $this->authService->getIdentity();
        $user = $this->em->getRepository(User::class)->findOneBy($parameter);
        return new ViewModel(['user' => $user]);
    }

    /**
     * @return \Zend\Http\Response|ViewModel|bool
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function editProfileAction()
    {
        $parameter['login'] = $this->authService->getIdentity();
        if(empty($parameter['login'])){
            return false;
        }
        $user = $this->em->getRepository(User::class)->findOneBy($parameter);
        $form = new EditProfileForm($this->em);
        $form->bind($user);

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $this->em->persist($user);
                $this->em->flush();
                return $this->redirect()->toRoute('user', ['action' => 'main']);
            }
        }
        return new ViewModel(['form' => $form]);
    }

    /**
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function changePasswordAction()
    {
        $parameter['login'] = $this->authService->getIdentity();
        if(empty($parameter['login'])){
            return false;
        }
        $user = $this->em->getRepository(User::class)->findOneBy($parameter);
        $form = new ChangePasswordForm($this->em);
        $form->bind($user);

        $bcrypt = new Bcrypt();

        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $user->setPassword($bcrypt->create($request->getPost()->password));
                $this->em->persist($user);
                $this->em->flush();
                return $this->redirect()->toRoute('user', ['action' => 'main']);
            }
        }
        return new ViewModel(['form' => $form]);
    }

    /**
     * @return ViewModel
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function resetPasswordAction()
    {
        $message = '';
        $form = new ResetPasswordForm($this->em);
        $user = new User();
        $form->bind($user);
        $uniqid = '';
        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $parameter['login'] = $request->getPost()->login;
                $userEntity = $this->em->getRepository(User::class)->findOneBy($parameter);
                if ($userEntity) {
                    $resetPassword = new ResetPassword();
                    $resetPassword->setLogin($userEntity);
                    $uniqid = uniqid();
                    $resetPassword->setToken($uniqid);
                    $this->em->persist($resetPassword);
                    $this->em->flush();

                    $html = 'Восстановить пароль: <a href="http://localhost/user/recovery-password/' . $uniqid . '">перейти</a>';
                    $m = new Message();
                    $m->addFrom('zend@localhost.ru', 'Zen Senov')
                        ->addTo('staspetryakov@gmail.com')
                        ->setSubject('Восстановление пароля');
                    $bodyPart = new MimeMessage();
                    $bodyMessage = new Part($html);
                    $bodyMessage->type = 'text/html';
                    $bodyPart->setParts(array($bodyMessage));
                    $m->setBody($bodyPart);
                    $m->setEncoding('UTF-8');
                    $transport = new Sendmail();
                    $transport->send($m);

                    $message = 'Письмо отправлено';
                }
            }
        }

        return new ViewModel(['form' => $form, 'time' => $uniqid, 'message' => $message]);
    }

    /**
     * @return \Zend\Http\Response|ViewModel
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function recoveryPasswordAction()
    {
        $form = new RecoveryPasswordForm($this->em);
        $parameter['token'] = $this->params('hash');

       $user = $this->em->getRepository(ResetPassword::class)->findOneBy($parameter);
        // $user = $this->em->getRepository(User::class)->find($resetPasswordEntity->getLogin()->getId());
        $form->bind($user);

        $bcrypt = new Bcrypt();
        /** @var Request $request */
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $user->getLogin()->setPassword($bcrypt->create($request->getPost()->password));
                $this->em->persist($user);
                $this->em->flush();

                return $this->redirect()->toRoute('user', ['action' => 'auth']);
            }
        }

        return new ViewModel(['form' => $form, 'params' => $this->params('hash')]);
    }

    public function testAction()
    {
        $rbac = new Rbac();
        $registered = new Role('registered');
        $rbac->addRole($registered);
        $registered->addPermission('view');
        $result = $rbac->isGranted($registered, 'view');
        //var_dump($result);
        //$bar = new Role('seven');
        //$foo->addChild($bar);
       // $rbac->addRole($bar);
        //$foo->addPermission('edit');
//        var_dump($rbac->getChildren());
        //var_dump($rbac->isGranted($bar, 'edit'));
      //  var_dump($rbac->isGranted($foo, 'edit'));

        return new ViewModel();
    }

    public function deniedAction()
    {
        /*if ($this->authService->getIdentity()) {
            $this->authService->clearIdentity();
        }*/
        return new ViewModel();
    }
}