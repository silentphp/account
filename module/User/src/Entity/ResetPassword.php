<?php
/**
 * Created by PhpStorm.
 * User: serv
 * Date: 01.03.18
 * Time: 15:19
 */

namespace User\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class ResetPasswordForm
 * @package User\Entitydd
 *
 * @ORM\Entity
 * @ORM\Table(name="reset_password")
 */
class ResetPassword
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="ResetPasswordForm")
     * @ORM\JoinColumn(name="login_id", referencedColumnName="id")
     */
    private $login;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $token;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @param mixed $login
     */
    public function setLogin($login)
    {
        $this->login = $login;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }
}