<?php
/**
 * Created by PhpStorm.
 * User: serv
 * Date: 26.02.18
 * Time: 17:38
 */

namespace User\Filter;


use Zend\InputFilter\InputFilter;

class AuthFilter extends InputFilter
{
    public function __construct()
    {
        $this->add([
            'name' => 'login',
            'required' => true
        ]);
        $this->add([
            'name' => 'password',
            'required' => true
        ]);
    }

}