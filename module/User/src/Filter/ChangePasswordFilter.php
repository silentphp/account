<?php
/**
 * Created by PhpStorm.
 * User: serv
 * Date: 01.03.18
 * Time: 10:50
 */

namespace User\Filter;


use Zend\InputFilter\InputFilter;

class ChangePasswordFilter extends InputFilter
{
    public function __construct()
    {
         $this->add([
            'name' => 'password',
            'required' => true,
            'validators' => [
                [
                    'name' => 'stringlength',
                    'options' => [
                        'min' => 3,
                        'messages' => array(
                            \Zend\Validator\StringLength::TOO_SHORT => 'Длина должна быть больше %min% символов',
                        ),
                    ],
                ]
            ]
        ]);
        $this->add([
            'name' => 'password_repeat',
            'required' => true,
            'validators' => [
                [
                    'name' => 'stringlength',
                    'options' => [
                        'min' => 3,
                        'messages' => array(
                            \Zend\Validator\StringLength::TOO_SHORT => 'Длина должна быть больше %min% символов',
                        ),
                    ],
                ],
                [
                    'name' => 'Identical',
                    'options' => [
                        'token' => 'password',
                        'messages' => array(
                            \Zend\Validator\Identical::NOT_SAME => 'Пароли не совпадают ',
                        ),
                    ],
                ]
            ]
        ]);
    }

}