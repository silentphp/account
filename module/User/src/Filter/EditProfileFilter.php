<?php
/**
 * Created by PhpStorm.
 * User: serv
 * Date: 28.02.18
 * Time: 14:48
 */

namespace User\Filter;


use Zend\InputFilter\InputFilter;

class EditProfileFilter extends InputFilter
{
    public function __construct()
    {
          $this->add([
            'name' => 'name',
            'required' => false,
            'validators' => [
                [
                    'name' => 'stringlength',
                    'options' => [
                        'min' => 3,
                        'messages' => array(
                            \Zend\Validator\StringLength::TOO_SHORT => 'Длина должна быть больше %min% символов',
                        ),
                    ],
                ],
              ]
          ]);
    }

}