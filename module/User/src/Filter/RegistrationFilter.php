<?php
/**
 * Created by PhpStorm.
 * User: serv
 * Date: 27.02.18
 * Time: 13:11
 */

namespace User\Filter;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use User\Entity\User;
use Zend\InputFilter\InputFilter;

class RegistrationFilter extends InputFilter
{
    public function __construct(EntityRepository $repository)
    {
        $this->add([
            'name' => 'login',
            'required' => true,
            'validators' => [
                [
                    'name' => 'stringlength',
                    'options' => [
                        'min' => 3,
                        'messages' => array(
                            \Zend\Validator\StringLength::TOO_SHORT => 'Длина должна быть больше %min% символов',
                        ),
                    ],
                ],
                [
                    'name' => 'DoctrineModule\Validator\NoObjectExists',
                    'options' => array(
                        'object_repository' => $repository,
                        'fields' => 'login',
                        'messages' => array(
                            \DoctrineModule\Validator\NoObjectExists::ERROR_OBJECT_FOUND => 'Логин %value% существует',
                        ),
                    ),
                ]
            ]
        ]);
        $this->add([
            'name' => 'password',
            'required' => true,
            'validators' => [
                [
                    'name' => 'stringlength',
                    'options' => [
                        'min' => 3,
                        'messages' => array(
                            \Zend\Validator\StringLength::TOO_SHORT => 'Длина должна быть больше %min% символов',
                        ),
                    ],
                ]
            ]
        ]);
        $this->add([
            'name' => 'password_repeat',
            'required' => true,
            'validators' => [
                [
                    'name' => 'stringlength',
                    'options' => [
                        'min' => 3,
                        'messages' => array(
                            \Zend\Validator\StringLength::TOO_SHORT => 'Длина должна быть больше %min% символов',
                        ),
                    ],
                ],
                [
                    'name' => 'Identical',
                    'options' => [
                        'token' => 'password',
                        'messages' => array(
                            \Zend\Validator\Identical::NOT_SAME => 'Пароли не совпадают ',
                        ),
                    ],
                ]
            ]
        ]);
    }

}