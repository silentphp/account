<?php
/**
 * Created by PhpStorm.
 * User: serv
 * Date: 25.02.18
 * Time: 0:33
 */

namespace User\Form;


use DoctrineModule\Stdlib\Hydrator\DoctrineObject;
use User\Entity\User;
use User\Filter\AuthFilter;
use Zend\Form\Form;

class AuthForm extends Form
{
    public function __construct($entityManager)
    {
        parent::__construct('user-form');
        $this->setAttribute('method', 'post');
        $this->setHydrator(new DoctrineObject($entityManager, User::class));
        $this->setInputFilter(new AuthFilter());
        $this->add([
            'type' => 'hidden',
            'name' => 'id',
        ]);
        $this->add([
            'type' => 'text',
            'name' => 'login',
            'attributes' => [
                'id' => 'login'
            ],
            'options' => [
                'label' => 'Логин',
            ],
        ]);
        $this->add([
            'type' => 'password',
            'name' => 'password',
            'attributes' => [
                'id' => 'password'
            ],
            'options' => [
                'label' => 'Пароль',
            ],
        ]);
        $this->add([
            'type' => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Авторизация',
                'id' => 'submitbutton',
            ],
        ]);
    }
}