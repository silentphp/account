<?php
/**
 * Created by PhpStorm.
 * User: serv
 * Date: 28.02.18
 * Time: 12:51
 */

namespace User\Form;


use Doctrine\ORM\EntityManager;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject;
use User\Entity\User;
use User\Filter\EditProfileFilter;
use Zend\Form\Form;

class EditProfileForm extends Form
{
    public function __construct(EntityManager $entityManager)
    {
        parent::__construct('user-form');
        $this->setAttribute('method', 'post');
        $this->setHydrator(new DoctrineObject($entityManager, User::class));
        $this->setInputFilter(new EditProfileFilter());
        $this->add([
            'type' => 'hidden',
            'name' => 'id',
        ]);
        $this->add([
            'type' => 'text',
            'name' => 'name',
            'attributes' => [
                'id' => 'name'
            ],
            'options' => [
                'label' => 'Имя',
            ],
        ]);
        $this->add([
            'type' => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Применить',
                'id' => 'submitbutton',
            ],
        ]);

    }

}