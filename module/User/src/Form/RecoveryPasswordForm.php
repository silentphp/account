<?php
/**
 * Created by PhpStorm.
 * User: serv
 * Date: 03.03.18
 * Time: 12:13
 */

namespace User\Form;


use Doctrine\ORM\EntityManager;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject;
use User\Entity\User;
use Zend\Form\Form;

class RecoveryPasswordForm extends Form
{
   public function __construct(EntityManager $entityManager)
   {
      parent::__construct('user-form');
        $this->setAttribute('method', 'post');
        $this->setHydrator(new DoctrineObject($entityManager, User::class));
        //$this->setInputFilter(new ChangePasswordFilter());

        $this->add([
            'type' => 'password',
            'name' => 'password',
            'attributes' => [
                'id' => 'password'
            ],
            'options' => [
                'label' => 'Password',
            ],
        ]);
        $this->add([
            'type' => 'password',
            'name' => 'password_repeat',
            'attributes' => [
                'id' => 'password_repeat'
            ],
            'options' => [
                'label' => 'Password repeat',
            ],
        ]);
        $this->add([
            'type' => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Применить',
                'id' => 'submitbutton',
            ],
        ]);
   }

}