<?php
/**
 * Created by PhpStorm.
 * User: serv
 * Date: 26.02.18
 * Time: 19:02
 */

namespace User\Form;


use Doctrine\ORM\EntityManager;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject;
use User\Entity\User;
use User\Filter\RegistrationFilter;
use Zend\Form\Form;

class RegistrationForm extends Form
{
    public function __construct(EntityManager $entityManager)
    {
        parent::__construct('user-form');
        $this->setAttribute('method', 'post');
        $this->setHydrator(new DoctrineObject($entityManager, User::class));
        $this->setInputFilter(new RegistrationFilter($entityManager->getRepository(User::class)));
        $this->add([
            'type' => 'hidden',
            'name' => 'id',
        ]);
        $this->add([
            'type' => 'text',
            'name' => 'login',
            'attributes' => [
                'id' => 'login'
            ],
            'options' => [
                'label' => 'login',
            ],
        ]);
        $this->add([
            'type' => 'password',
            'name' => 'password',
            'attributes' => [
                'id' => 'password'
            ],
            'options' => [
                'label' => 'Password',
            ],
        ]);
        $this->add([
            'type' => 'password',
            'name' => 'password_repeat',
            'attributes' => [
                'id' => 'password_repeat'
            ],
            'options' => [
                'label' => 'Password repeat',
            ],
        ]);
        $this->add([
            'type' => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Регистрация',
                'id' => 'submitbutton',
            ],
        ]);
    }
}