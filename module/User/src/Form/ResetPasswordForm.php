<?php
/**
 * Created by PhpStorm.
 * User: serv
 * Date: 01.03.18
 * Time: 17:11
 */

namespace User\Form;


use Doctrine\ORM\EntityManager;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject;
use User\Entity\User;
use Zend\Form\Form;

class ResetPasswordForm extends Form
{
    public function __construct(EntityManager $entityManager)
    {
        parent::__construct('user-form');
        $this->setAttribute('method', 'post');
        $this->setHydrator(new DoctrineObject($entityManager, User::class));

        $this->add([
            'type' => 'text',
            'name' => 'login',
            'attributes' => [
                'id' => 'login'
            ],
            'options' => [
                'label' => 'Login'
            ]
        ]);
        $this->add([
           'type' => 'submit',
           'name' => 'submit',
           'attributes' => [
               'value' => 'Восстановить',
               'id' => 'submitbutton'
           ]
        ]);
    }
}