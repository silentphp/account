<?php
/**
 * Created by PhpStorm.
 * User: serv
 * Date: 21.02.18
 * Time: 11:29
 */

namespace User;


use User\Service\AuthManager;
use User\Service\RbacManager;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\MvcEvent;

class Module
{
    const VERSION = '3.0.3-dev';

    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    public function onBootstrap(MvcEvent $event)
    {
        // Get event manager.
        $eventManager = $event->getApplication()->getEventManager();
        $sharedEventManager = $eventManager->getSharedManager();
        // Register the event listener method.
        $sharedEventManager->attach(AbstractActionController::class,MvcEvent::EVENT_DISPATCH, [$this, 'onDispatch']);
    }


    /**
     * @param MvcEvent $event
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function onDispatch(MvcEvent $event)
    {
        $controller = $event->getTarget();
        $controllerName = $event->getRouteMatch()->getParam('controller', null);
        $actionName = $event->getRouteMatch()->getParam('action', null);
//        $actionName = str_replace('-', '', lcfirst(ucwords($actionName, '-')));

        $authManager = $event->getApplication()->getServiceManager()->get(AuthManager::class);
        $result = $authManager->filterAccess($controllerName, $actionName);
        //file_put_contents($_SERVER['DOCUMENT_ROOT']. '/ss.log', var_export($result, true), FILE_APPEND);
        if($result == RbacManager::ACCESS_NEED){
            return $controller->redirect()->toRoute('user', ['action' => 'auth']);
        } elseif ($result == RbacManager::ACCESS_DENIED){
            return $controller->redirect()->toRoute('user', ['action' => 'denied']);
        }
    }
}