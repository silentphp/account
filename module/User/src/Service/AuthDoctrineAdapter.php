<?php
/**
 * Created by PhpStorm.
 * User: serv
 * Date: 24.02.18
 * Time: 0:39
 */

namespace User\Service;


use Doctrine\ORM\EntityManager;
use User\Entity\User;
use Zend\Authentication\Adapter\AdapterInterface;
use Zend\Authentication\Result;
use Zend\Crypt\Password\Bcrypt;

class AuthDoctrineAdapter implements AdapterInterface
{
    public $em;

    private $login;

    private $password;

    public function __construct(EntityManager $entityManager, $login, $password)
    {
        $this->em = $entityManager;
        $this->login = $login;
        $this->password = $password;
    }

    public function authenticate()
    {
        $parameter['login'] = $this->login;
        $user = $this->em->getRepository(User::class)->findOneBy($parameter);
        if ($user == null) {
            return new Result(Result::FAILURE_IDENTITY_NOT_FOUND, null, ['Логин или пароль неверен']);
        }
        $bcrypt = new Bcrypt();
        if($bcrypt->verify($this->password, $user->getPassword())){
            return new Result(Result::SUCCESS, $this->login, ['Авторизация прошла успешно']);
        } else {
            return new Result(Result::FAILURE_CREDENTIAL_INVALID, null, ['Пароль неверен']);
        }
    }
}