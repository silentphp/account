<?php
/**
 * Created by PhpStorm.
 * User: serv
 * Date: 10.03.18
 * Time: 9:18
 */

namespace User\Service;


class AuthManager
{
    /**
     * @var RbacManager
     */
    private $rbacManager;

    public function __construct($config, $rbacManager)
    {
        $this->config = $config;
        $this->rbacManager = $rbacManager;
    }

    public function filterAccess($controllerName, $actionName)
    {
        if (in_array($controllerName, $this->config['controllers'])) {
            $items = $this->config['controllers'][$controllerName];
            if (isset($items)) {
                foreach ($items as $item) {
                    $actionList = $item['actions'];
                    $allow = $item['allow'];
                    if (is_array($actionList) && in_array($actionName, $actionList)) {
                        return $this->rbacManager->checkGranted($allow);
                    }
                }
            }
        }
    }

}