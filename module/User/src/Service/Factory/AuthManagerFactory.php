<?php
/**
 * Created by PhpStorm.
 * User: serv
 * Date: 10.03.18
 * Time: 9:32
 */

namespace User\Service\Factory;


use Interop\Container\ContainerInterface;
use User\Service\AuthManager;
use User\Service\RbacManager;
use Zend\ServiceManager\Factory\FactoryInterface;

class AuthManagerFactory implements FactoryInterface
{

    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return object|AuthManager
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('Config');
        $rbacManager = $container->get(RbacManager::class);

        if (isset($config['access_filter'])){
            $config = $config['access_filter'];
        }

        return new AuthManager($config, $rbacManager);
    }
}