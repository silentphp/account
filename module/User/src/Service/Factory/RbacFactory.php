<?php
/**
 * Created by PhpStorm.
 * User: serv
 * Date: 11.03.18
 * Time: 20:52
 */

namespace User\Service\Factory;


use Interop\Container\ContainerInterface;
use Zend\Permissions\Rbac\Rbac;
use Zend\ServiceManager\Factory\FactoryInterface;

class RbacFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new Rbac();
    }

}