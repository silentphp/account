<?php
/**
 * Created by PhpStorm.
 * User: serv
 * Date: 11.03.18
 * Time: 0:39
 */

namespace User\Service\Factory;


use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use User\Service\RbacManager;
use Zend\Authentication\AuthenticationService;
use Zend\Permissions\Rbac\Rbac;
use Zend\ServiceManager\Factory\FactoryInterface;

class RbacManagerFactory implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return object|RbacManager
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $authService = $container->get(AuthenticationService::class);
        $entityManager = $container->get(EntityManager::class);
        $rbac = $container->get(Rbac::class);

        return new RbacManager($authService, $entityManager, $rbac);
    }

}