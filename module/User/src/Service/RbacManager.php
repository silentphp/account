<?php
/**
 * Created by PhpStorm.
 * User: serv
 * Date: 11.03.18
 * Time: 0:37
 */

namespace User\Service;


use Doctrine\ORM\EntityManager;
use User\Entity\User;
use Zend\Authentication\AuthenticationService;
use Zend\Permissions\Rbac\Rbac;
use Zend\Permissions\Rbac\Role;

class RbacManager
{
    const ACCESS_NEED = 1;
    const ACCESS_DENIED = 2;
    const ACCESS_OK = 3;
    /**
     * @var AuthenticationService
     */
    private $authService;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var Rbac
     */
    private $rbac;

    public function __construct($authService, $entityManager, $rbac)
    {
        $this->authService = $authService;
        $this->em = $entityManager;
        $this->rbac = $rbac;
    }

    public function init()
    {
        $registered = new Role('registered');
        $admin = new Role('admin');
        $admin->addChild($registered);
        //$this->rbac->addRole($registered);
        $this->rbac->addRole($admin);
        $registered->addPermission('view');
        $admin->addPermission('edit');
    }

    public function checkGranted($permission)
    {
        $this->init();
        if (!$this->authService->getIdentity()) {
            return self::ACCESS_NEED;
        }
        $parameter['login'] = $this->authService->getIdentity();
        $user = $this->em->getRepository(User::class)->findOneBy($parameter);
        if (empty($user->getRole())){
            return self::ACCESS_DENIED;
        }
        if($this->rbac->isGranted($user->getRole(), $permission)){
            return self::ACCESS_OK;
        }
        else {
            return self::ACCESS_DENIED;
        }
    }
}